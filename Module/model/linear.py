import pandas, numpy

version_string = '1.0.1'

def version():
    # Define your detail Module function here
    print(version_string)


def multi_dataframe_merge(df_list: list, on_list: list, how: str = 'inner') -> pd.DataFrame:
    """
    merge the dataframe with given column list and method.
    author: Donglai Jiang
    :arg: df_list       list: [df1, df2, ...]
    :arg: on_list       list: [str1, str2, ...]
    :arg: how           str: 'inner', 'left', ....
    :return: pd.DataFrame
    """
    from functools import reduce
    import pandas as pd

    df_final = reduce(lambda left, right: pd.merge(left, right, on=on_list, how=how), df_list)
    return df_final


class Message:
    """
    A class may contains the all kinds of messages.
    author: Donglai Jiang
    """
    def __init__(self, m_type: str):
        self.m_type = m_type
    

class LogMessage(Message):
    """
    A class that inherits Message and may store your log files of to your work path.
    author: Donglai Jiang
    """
    
    def __init__(self, log: str):
        super().__init__("LOG")
        self.log = log

    def do_log(self, path):
        """
        At the very moment, print and save your log message.
        :arg: path          your log file path, like 'xxxxxx/cccc/lll.log'
        :return: 
        """
        from datetime import datetime
        
        
        logging_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        log_file = open(path, 'a')
        log_prefix = "LOG at time " + logging_time + ": "
        print(log_prefix + self.log)
        log_file.write(log_prefix + self.log + "\n")
        log_file.close()
        
        
def get_rm(index_codes: list, start: str, end: str, root_path: str) -> pd.DataFrame:
    """
    Get the Chinese market return from the website  'http://quotes.money.163.com/trade/lsjysj_zhishu' 
    and save to dataframe. Your can choose list of market indexes to get the average market return.
    The start and end are date range you choose to get.
    :arg: index_codes       list: ['000001.SH', '399001.SZ', ...]
    :arg: start             string      "2016-12-01"
    :arg: end               string      "2020-10-29"
    :arg: root_path         string      Your root path
    :return: pd.DataFrame
    author: Donglai Jiang
    """
    from functools import reduce
    import pandas as pd
    import datetime
    
    
    index_df_list = []
    for code in index_codes:
        df_list = []
        for year in range(int(start[:4]), int(end[:4]) + 1):
            for season in range(1, 5):
                url = 'http://quotes.money.163.com/trade/lsjysj_zhishu_{}.html?year={}&season={}'.format(code[:6], year,
                                                                                                         season)
                while True:
                    try:
                        hist_price_page = requests.get(url)
                        print('Success!!!')
                        df = pd.read_html(hist_price_page.text)[3]
                        df['日期'] = pd.to_datetime(df['日期'], format='%Y%m%d')
                        df = df[['日期', '涨跌幅(%)']]
                        df_list.append(df)
                        print(df.head())
                        time.sleep(0.5)
                        break
                    except Exception as e:
                        print(e)
                        time.sleep(0.2)
        df = pd.concat(df_list, axis=0)
        df.sort_values(by='日期', inplace=True)
        index_df_list.append(df)
    df_final = reduce(lambda left, right: pd.merge(left, right, on='日期'), index_df_list)
    df_final['PCT_CHG'] = df_final.iloc[:, 1:].mean(axis=1)
    df_final.rename(columns={'日期': 'DATE'}, inplace=True)
    df_final = df_final[['DATE', 'PCT_CHG']]
    start = datetime.datetime.strptime(start, '%Y/%m/%d')
    end = datetime.datetime.strptime(end, '%Y/%m/%d')
    df_final = df_final.loc[(df_final['DATE'] >= start) & (df_final['DATE'] <= end), :]
    df_final.to_csv(os.path.join(root_path, 'index_return.csv'), index=False)
    return df_final